let express = require('express');
let bodyParser = require('body-parser');
let fs = require('fs');
let request = require('request');
const path = require('path');
const Tabletop = require('tabletop'); //arjunvenkatraman added to load data from Google Sheets directly
let arrayWithData = [];
const app = express();
const port = process.env.PORT || 5000;
const datasrc = "SHEET" // "TSV" or "SHEET"
const srcsheetlabel = "Sheet1"
let cors = require('cors');
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
const publicSpreadsheetUrl = "https://docs.google.com/spreadsheets/d/1mWMpaqdFO3uE51TzlbT1pzoOQLOHTjzAGQGhZfGIwLk/edit#gid=0";

// Datasource check with datasrc var
/*
app.get('/getVideoData', async (req, res) => {
  if (datasrc === "TSV") {
    let rawtsv = fs.readFileSync('./RawData/VideoData.tsv', 'utf8')
    let revisedJSON = await tsvJSON(rawtsv);
    fs.writeFileSync('./RawData/VideoData.json', JSON.stringify(revisedJSON, null, 2))
    console.log("Sending back TSV Response")
    res.send(revisedJSON)
  }
  if (datasrc === "SHEET") {
    let revisedJSON = await getSheetData();
    fs.writeFileSync('./RawData/VideoData.json', JSON.stringify(revisedJSON, null, 2))
    console.log("Sending Sheet Response")
    console.log(revisedJSON)
    res.send(revisedJSON)
  }

})
*/
app.get('/members', async (req, res) => {
  /*
  responseJSON = {
    terry: 'maka'
  }
  */
  console.log("Getting members!")
  let responseJSON = await getSheetData("members");
  console.log(responseJSON)

  res.send(responseJSON)
})
app.get('/partners', async (req, res) => {
  /*
  responseJSON = {
    terry: 'maka'
  }
  */
  console.log("Getting partners!")
  let responseJSON = await getSheetData("partners");
  console.log(responseJSON)

  res.send(responseJSON)
})

app.get('/projects', async (req, res) => {
  /*
  responseJSON = {
    terry: 'maka'
  }
  */
  console.log("Getting projects!")
  let responseJSON = await getSheetData("projects");
  console.log(responseJSON)

  res.send(responseJSON)
})
// Pulling from Google Sheets with Tabletop
function getSheetData(sheetlabel) {
  return new Promise((resolve) => {
    Tabletop.init({
      key: publicSpreadsheetUrl,
      callback: function(data, tabletop) {
        resolve(processSheetData(sheetlabel, data, tabletop));
      },
      simpleSheet: false
    })
  })
}

//Cleaning up the sheet data
function processSheetData(sheetlabel, data, tabletop) {
    if (!(sheetlabel in data)){
      return(sheetlabel+" Not found in sheet")
    }
    else{
      data = data[sheetlabel].elements
    }
    let newjson = {"totalElements":0}
    newjson[sheetlabel]={}
    data.map(currentline => {
        newjson[sheetlabel][currentline['name']]=currentline;
    })
    newjson.totalElements = data.length;
    return (newjson)
}

//Cleaning up the TSV data
function tsvJSON(tsv) {
  return new Promise((resolve, reject) => {
    var lines = tsv.split(/\r?\n/);
    let titleLine = lines.shift();
    let captionIndex = titleLine.split(/\t/).indexOf('Caption');
    let dateIndex = titleLine.split(/\t/).indexOf('Date');
    let latIndex = titleLine.split(/\t/).indexOf('Latitude (°N)');
    let longIndex = titleLine.split(/\t/).indexOf('Longitude (°E)');
    let linkIndex = titleLine.split(/\t/).indexOf('Link');
    let cityIndex = titleLine.split(/\t/).indexOf('City');
    let newjson = {"cities":{},"totalVideos":0}

    lines.map(line => {
        let currentline = line.split(/\t/);
        if(!isNaN(currentline['Latitude (°N)']) && !isNaN(currentline['Longitude (°E)'])) {
            if(newjson.cities[currentline[cityIndex]] != undefined) {
                newjson.cities[currentline[cityIndex]].videos.push({
                    link: currentline[linkIndex],
                    caption: currentline[captionIndex],
                    date: currentline[dateIndex]
                })
            }
            else {
                newjson.cities[currentline[cityIndex]] = {
                    videos: [{
                        link: currentline[linkIndex],
                        caption: currentline[captionIndex],
                        date: currentline[dateIndex]
                    }],
                    coordinates: {
                        latitude: currentline[latIndex],
                        longitude: currentline[longIndex]
                    }
                }
            }
        }
    })
    newjson.totalVideos = lines.length;
    resolve(newjson);
    // reject({
    //   error: 'something went wrong in tsv to JSON conversion'
    // })
  })
}

if (process.env.NODE_ENV === 'production') {
  // Serve any static files
  app.use(express.static(path.join(__dirname, 'client/build')));

  // Handle React routing, return all requests to React app
  app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
  });
}

app.listen(port, () => console.log(`Listening on port: ${port}`));


//TWITTER EMBED API INFO
// app.post('/getTwitterEmbedInfo', async (req, res) => {
//     console.log("inside getTwitterEmbedInfo")
//     let reqUrl = await buildUrl('https://publish.twitter.com/oembed', {url: req.body.url,theme: 'dark',widget_type: 'video'})
//     request( {url: reqUrl}, (err, resp, body) => {
//         let bodyJSON = JSON.parse(body);
//         console.log(bodyJSON)
//         res.send(bodyJSON)
//     })
// })

// function buildUrl(url, parameters) {
//     return new Promise((resolve, reject) => {
//         let qs = "";
//         for (const key in parameters) {
//             if (parameters.hasOwnProperty(key)) {
//                 const value = parameters[key];
//                 qs +=
//                     encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
//             }
//         }
//         if (qs.length > 0) {
//             qs = qs.substring(0, qs.length - 1); //chop off last "&"
//             url = url + "?" + qs;
//         }
//         console.log(url);
//         resolve(url);
//     })
// }
